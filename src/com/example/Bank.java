package com.example;
import java.util.*;
public class Bank {
    private float minimumBalance=1000;
    private float withdrawAmout=0;
    public void inputFromUser(){
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter amount to widthdraw:");
        withdrawAmout=scanner.nextFloat();
    }

    public void checkMinimumBalance() throws MyException {
        if(withdrawAmout >= minimumBalance){
            throw new MyException("cannot withdraw less than  minimum balance");
        }
    }
}
