package com.example;

public class Main {
    public static void main(String[] args)   {
        Bank bank =new Bank();
        bank.inputFromUser();
        try {
            bank.checkMinimumBalance();
        }catch (MyException e){
            System.out.println(e.getMessage());
        }
    }
}
